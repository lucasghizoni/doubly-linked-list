# Doubly linked list

## Contents

- Description
- Usage
- Requirements
- API
    - addFirst
    - addLast
    - getFirst
    - getLast
    - removeFirst
    - removeLast
    - remove
    - getAt
    - toArray
- Tests
- Publishing

## Description

A lightweight, fast and powerful Typescript library to work with [Doubly linked lists](https://en.wikipedia.org/wiki/Doubly_linked_list). Compatible with server-side environments like Node.js.

## Usage

Install:
```sh
npm install doubly-linked-list
```

In your code:

```js
import { DoublyLinkedList } from 'doubly-linked-list';

const doublyLinkedList = new DoublyLinkedList<number>();
doublyLinkedList.addFirst(1);
```


## Requirements

The Doubly Linked List library has zero dependencies.

## API

### addFirst(item: T): void function
Adds a new item in the first position of the list.

### addLast(item: T): void function
Adds a new item in the last position of the list.

### getFirst(): T function
Gets the first item of the list.

### getLast(): T function
Gets the last item of the list.

### removeFirst(): T function
Removes the first item of the list.

### removeLast(): T function
Removes the last item of the list.

### remove(item: T): boolean function
Remove a certain item in the list.

### getAt(position: number): T function
Gets a certain item in the list.

### toArray(): T[]
Parses the list to an array.

## Tests

```sh
npm run test
```

Check the coverage in the "coverage" folder.

## Publishing

```sh
npm publish
```

It will automatically run the tests before, and then publish in the npm repo.


## License

The Doubly Linked List library is released under the
[MIT license](https://opensource.org/licenses/MIT).
