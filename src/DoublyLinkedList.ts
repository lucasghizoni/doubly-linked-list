import { deepEqual } from "./deep-equal";

class Node<T> {

  // Reference to the previous node
  previous?: Node<T>;
  
  // The item of this node
  item: T;

  // Reference to the next node
  next?: Node<T>;

  /**
  * Creates a new Node instance for the given item.
  *
  * @param item - new item
  */
  constructor(item: T) {
    this.item = item;
  }
}

export class DoublyLinkedList<T> {
  
  // First item of the list.
  private first: Node<T>;
  
  // Last item of the list.
  private last: Node<T>;
  
  // Length of the list.
  public length: number = 0;


  /**
  * Adds a new item in the first position of the list.
  *
  * @param item - new item
  */
  addFirst(item: T): void {
    const node = new Node<T>(item);

    if(!this.first) {
      this.first = node;
      this.last = node;
      return;
    }

    node.next = this.first;
    this.first.previous = node;
    this.first = node;
    this.length++;
  }


  /**
  * Adds a new item in the last position of the list.
  *
  * @param item - new item
  */
  addLast(item: T): void {
    const node = new Node<T>(item);

    if(!this.first) {
      this.first = node;
      this.last = node;
      return;
    }

    node.previous = this.last;
    this.last.next = node;
    this.last = node;
    this.length++;
  }


  /**
  * Gets the first item of the list.
  *
  * @returns first item of the list
  */
  getFirst(): T {
    return this.first ? this.first.item : null;
  }


  /**
  * Gets the last item of the list.
  *
  * @returns last item of the list
  */
  getLast(): T {
    return this.last ? this.last.item : null;
  }


  /**
  * Removes the first item of the list
  *
  * @returns item removed. null if list is empty
  */
  removeFirst(): T {
    if(!this.first) {
      return null;
    }

    const oldFirst = this.first.item;
    this.first = this.first.next;
    
    if(this.first) {
      this.first.previous = null;
    }
    
    this.length--;

    return oldFirst;
  }


  /**
  * Removes the last item of the list
  *
  * @returns item removed
  */
  removeLast(): T {
    if(!this.last){
      return null;
    }

    const oldLast = this.last.item;
    this.last = this.last.previous;
    
    if(this.last) {
      this.last.next = null;
    }
    
    this.length--;
    
    return oldLast;
  }


  /**
  * Remove a certain item in the list.
  *
  * @param item - the item to be removed
  * 
  * @returns true if item was removed. false if not.
  */
  remove(item: T): boolean {
    let current = this.first;
    let foundItem = false;

    while (current) {
      if (deepEqual(current.item, item)) {
        if(current.next) {
          current.next.previous = current.previous;
        }

        if(current.previous) {
          current.previous.next = current.next;
        }
        
        foundItem = true;
        this.length--;
        break;
      }
      current = current.next;
    }

    return foundItem;
  }


  /**
  * Gets a certain item in the list.
  *
  * @param position - position of an item in the list
  * 
  * @returns item
  */
  getAt(position: number): T {
    if(this.length < position - 1) {
      return null;
    }

    let current = this.first;
    let counter = 0;

    while (current) {
      if(position === counter) {
        break;
      }
      counter++;
      current = current.next;
    }
    return current.item;
  }

  /**
  * Parses the list to an array.
  * 
  * @returns Array of items
  */
  toArray(): T[] {
    let current = this.first;
    let itemsArray = [];

    while(current){
      itemsArray.push(current.item);
      current = current.next;
    }

    return itemsArray;
  }
}