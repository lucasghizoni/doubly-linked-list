import { strict as assert } from 'assert';

/**
* Compare two values of any type (boolean, number, string, object, etc...).
*
* @param value1 - first value
* @param value2 - second value
* 
* @returns true if they are deep equal. false if not.
*/
export function deepEqual(value1: any, value2: any): boolean {
  try {
    assert.deepEqual(value1, value2);
    return true;
  } catch (e) {
    return false;
  }
}