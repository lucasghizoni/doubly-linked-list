import { DoublyLinkedList } from '../src/index';

describe('DoublyLinkedList.addFirst', () => {
  test("if item is first added to list, must be placed in first and last pos ", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();
    const item = 1;
    
    doublyLinkedList.addFirst(item);

    expect(doublyLinkedList.getFirst()).toBe(item);
    expect(doublyLinkedList.getLast()).toBe(item);
  });

  test("addFirst is called in a list that has already items, previous firstItem must be at second position", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();
    const item = 1;
    const secondItemAdded = 2;
    
    doublyLinkedList.addFirst(item);
    doublyLinkedList.addFirst(secondItemAdded);

    expect(doublyLinkedList.getAt(1)).toBe(item);
  });
});

describe('DoublyLinkedList.addLast', () => {
  test("if item is first added to list, must be placed in first and last pos ", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();
    const item = 1;
    
    doublyLinkedList.addLast(item);

    expect(doublyLinkedList.getFirst()).toBe(item);
    expect(doublyLinkedList.getLast()).toBe(item);
  });

  test("When addLast is called in a list that has already items, last position must be updated with new item", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();
    const item = 1;
    const secondItemAdded = 2;
    
    doublyLinkedList.addLast(item);
    doublyLinkedList.addLast(secondItemAdded);

    expect(doublyLinkedList.getLast()).toBe(secondItemAdded);
  });
});

describe('DoublyLinkedList.getFirst', () => {
  test("getFirst must return first item of list", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();
    const item = 1;
    const secondItemAdded = 2;
    
    doublyLinkedList.addFirst(item);
    doublyLinkedList.addFirst(secondItemAdded);

    expect(doublyLinkedList.getFirst()).toBe(secondItemAdded);
  });

  test("getFirst returns null if list is empty", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();

    expect(doublyLinkedList.getFirst()).toBe(null);
  });
});

describe('DoublyLinkedList.getLast', () => {
  test("getLast must return last item of list", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();
    const item = 1;
    const secondItemAdded = 2;
    
    doublyLinkedList.addLast(item);
    doublyLinkedList.addLast(secondItemAdded);

    expect(doublyLinkedList.getLast()).toBe(secondItemAdded);
  });

  test("returns null if list is empty", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();

    expect(doublyLinkedList.getLast()).toBe(null);
  });
});

describe('DoublyLinkedList.removeFirst', () => {
  test("removes the first item of the list", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();
    const insertedItem = 1;

    doublyLinkedList.addFirst(insertedItem);
    const removedItem = doublyLinkedList.removeFirst();

    expect(removedItem).toBe(insertedItem);
  });

  test("removes the correct first item", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();
    const insertedItem = 1;
    const insertedTwo = 2;

    doublyLinkedList.addFirst(insertedItem);
    doublyLinkedList.addFirst(insertedTwo);
    const removedItem = doublyLinkedList.removeFirst();

    expect(removedItem).toBe(insertedTwo);
  });

  test("returns null if list is empty", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();

    expect(doublyLinkedList.removeFirst()).toBe(null);
  });
});

describe('DoublyLinkedList.removeLast', () => {
  test("removes the last item of the list", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();
    const insertedItem = 1;

    doublyLinkedList.addLast(insertedItem);
    const removedItem = doublyLinkedList.removeLast();

    expect(removedItem).toBe(insertedItem);
  });

  test("removes the correct last item", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();
    const insertedItem = 1;
    const insertedTwo = 2;

    doublyLinkedList.addLast(insertedItem);
    doublyLinkedList.addLast(insertedTwo);
    const removedItem = doublyLinkedList.removeLast();

    expect(removedItem).toBe(insertedTwo);
  });

  test("returns null if list is empty", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();

    expect(doublyLinkedList.removeLast()).toBe(null);
  });
});

describe('DoublyLinkedList.remove', () => {
  test("removes certain item of the list", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();
    const insertedItem = 1;
    const insertedTwo = 2;

    doublyLinkedList.addFirst(insertedItem);
    doublyLinkedList.addFirst(insertedTwo);
    
    const wasRemoved = doublyLinkedList.remove(insertedItem);

    expect(wasRemoved).toBe(true);
  });

  test("removes certain item of the list in the first position", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();
    const insertedItem = 1;
    const insertedTwo = 2;

    doublyLinkedList.addFirst(insertedItem);
    doublyLinkedList.addFirst(insertedTwo);
    
    const wasRemoved = doublyLinkedList.remove(insertedTwo);

    expect(wasRemoved).toBe(true);
  });

  test("returns null if list is empty", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();

    expect(doublyLinkedList.removeLast()).toBe(null);
  });
});

describe('DoublyLinkedList.getAt', () => {
  test("get item of given position", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();
    const insertedItem = 1;

    doublyLinkedList.addFirst(insertedItem);

    expect(doublyLinkedList.getAt(0)).toBe(insertedItem);
  });

  test("returns null if position does not exist on list", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();
    doublyLinkedList.addFirst(1);
    doublyLinkedList.addFirst(2);

    expect(doublyLinkedList.getAt(8)).toBe(null);
  });
});

describe('DoublyLinkedList.toArray', () => {
  test("parses the created list to an array", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();

    doublyLinkedList.addFirst(1);
    doublyLinkedList.addFirst(2);

    expect(doublyLinkedList.toArray()).toEqual([2,1]);
  });

  test("returns null if position does not exist on list", () => {
    const doublyLinkedList = new DoublyLinkedList<number>();
    doublyLinkedList.addFirst(1);
    doublyLinkedList.addFirst(2);

    expect(doublyLinkedList.getAt(8)).toBe(null);
  });
});