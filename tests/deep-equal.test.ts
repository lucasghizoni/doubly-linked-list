import { deepEqual } from "../src/deep-equal";

describe('deepEqual', () => {
  test("if two values are the same, must return true ", () => {
    expect(deepEqual({prop1: 1}, {prop1: 1})).toBe(true);
  });

  test("if two values are not the same, must return true ", () => {
    expect(deepEqual({prop1: 1}, {prop1: 2})).toBe(false);
  });
});